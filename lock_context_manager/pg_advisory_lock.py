import psycopg2
import sys
import hashlib
import collections

class PgAdvisoryLock:
    def __init__(self, dsn):
        self._locks = collections.defaultdict(lambda: 0)
        self.dsn = dsn

    def __enter__(self):
        self.connection = psycopg2.connect(self.dsn)
        self.cursor = self.connection.cursor()
        return self

    def __exit__(self, xc_type, exc, trace):
        self.cursor.close()
        self.connection.close()

    def _enforce_bytes(self, key):
        if isinstance(key, str):
            return key.encode()
        elif isinstance(key, bytes):
            return key
        else:
            raise TypeError(
                    f'Unhashable type {type(key)}, str or bytes required')

    def _make_key(self, key):
        return int.from_bytes(
                hashlib.blake2b(self._enforce_bytes(key),
                                digest_size=8).digest(),
                byteorder=sys.byteorder,
                signed=True)

    def acquire(self, identity, blocking=True):
        """Returns True as lock is acquired.

        Multiple lock requests stacks."""
        if blocking:
            self.cursor.execute('SELECT pg_advisory_lock(%s);',
                                (self._make_key(identity),))
            self._locks[self._enforce_bytes(identity)] += 1
            return True
        else:
            self.cursor.execute('SELECT pg_try_advisory_lock(%s);',
                                (self._make_key(identity),))
            r = self.cursor.fetchone()
            if r[0] is True:
                self._locks[self._enforce_bytes(identity)] += 1
            return r[0]


    def release(self, identity):
        """Returns True as long as lock is held."""
        self.cursor.execute('SELECT pg_advisory_unlock(%s);',
                            (self._make_key(identity),))
        r = self.cursor.fetchone()
        if r[0] is True:
            self._locks[self._enforce_bytes(identity)] -= 1
        return r[0]

    @property
    def locks(self):
        return dict(self._locks)
