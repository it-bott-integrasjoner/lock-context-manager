def get_lock(dsn):
    if dsn.startswith('postgresql'):
        from .pg_advisory_lock import PgAdvisoryLock
        return PgAdvisoryLock(dsn)
    else:
        raise NotImplementedError(f'No locker implemented for {dsn.split(":")[0]}')

def locker(key_generator, dsn_resolver):
    def wrapper(f):
        def call(*args, **kwargs):
            lock_dsn = dsn_resolver(*args, **kwargs) if callable(dsn_resolver) else dsn_resolver
            if not lock_dsn:
                return f(*args, **kwargs)
            else:
                with get_lock(lock_dsn) as l:
                    l.acquire(key_generator(*args, **kwargs))
                    r = f(*args, **kwargs)
                    l.release(key_generator(*args, **kwargs))
                    return r
        return call
    return wrapper
