import concurrent.futures
import time
import os
import pytest

from lock_context_manager import get_lock

def create_lock():
    return get_lock(
            'postgresql://postgres:postgres@{host}:{port}/lcm-test'.format(
                host=os.environ.get('POSTGRES_HOST'),
                port=os.environ.get('POSTGRES_5432_TCP_PORT')))


def test_lock_acquire():
    with create_lock() as l1:
        with create_lock() as l2:
            l1.acquire('my_key')
            assert l2.acquire('my_key', blocking=False) == False


def test_lock_release():
    with create_lock() as l1:
        with create_lock() as l2:
            l1.acquire('my_key')
            l1.release('my_key')
            assert l2.acquire('my_key', blocking=False) == True


def get_pg_locks(c):
        c.execute("SELECT granted from pg_locks where locktype = 'advisory'")
        return [x[0] for x in c]


def test_blocking_lock_acquire():
    """Acquire two locks, and check that one of the advisory locks is not
    granted, and that the non-granted lock is running"""
    with create_lock() as l1:
        l1.acquire('l')
        def ac():
            with create_lock() as l2:
                return l2.acquire('l')

        # Acquire a blocking lock
        with concurrent.futures.ThreadPoolExecutor() as pool:
            r = pool.submit(ac)

            # The following section spin-sleep-wait until the blocking lock is
            # actually acquired
            locks = get_pg_locks(l1.cursor)
            i = 0
            while (r.running() is not True or
                len(locks) != 2):
                print(i)
                if i == 10:
                    break
                else:
                    i += 1
                # This seems to be the optimum time to sleep for the future to start
                time.sleep(0.003)
                locks = get_pg_locks(l1.cursor)

            # Assert that two locks are requested, one is granted, and that the
            # blocked requestor is still running
            locks = get_pg_locks(l1.cursor)
            assert len(locks) == 2
            assert True in locks and False in locks
            assert r.running() == True

            # Let the lock that is blocking other locks, and spin-sleep-wait for the
            # requestor exit
            l1.release('l')
            while r.running() is True:
                time.sleep(0.003)
                pass

            # Check that all locks has been released
            locks = get_pg_locks(l1.cursor)
            assert len(locks) == 0
