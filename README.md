
# Lock context manager

Simply acquire a lock?

    from lock_context_manager import get_lock
    dsn = 'postgresql://'
    with get_lock(dsn) as lock:
        identity = 'my-resource-is-very-special-and-fragile'
        lock.acquire(identity)
        lock.locks #  == {b'my-resource-is-very-special-and-fragile': 1}
        do(identity)
        lock.release(identity)
        lock.locks #  == {b'my-resource-is-very-special-and-fragile': 0}

Or

    from lock_context_manager import locker

    def dsn_resolver(self, *args, **kwargs):
        return self.config.lock_dsn

    class MyClass:
        def __init__(self):
            self.lock_dsn = 'postgresql://'

        @staticmethod
        def identity(*args, **kwargs):
            return '-'.join(kwargs.keys())

        @locker(MyClass.identity, dsn_resolver)
        def do_fragile_stuff(self):
            pass

Multiple calls to acquire() stacks, lock is totally released when release()
returns False, or the context manager exits.



repo cloned from https://bitbucket.usit.uio.no/projects/KAL/repos/lock-context-manager

